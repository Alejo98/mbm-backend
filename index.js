const express = require('express');
const bodyParser = require('body-parser'); // Para analizar el cuerpo de la solicitud POST
const { Pool } = require('pg');
const mysql = require('mysql');
const axios = require('axios'); // Importa Axios
const cors = require('cors');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt")
const session = require("express-session")
const flash = require("express-flash")
const multer = require('multer');
const crypto = require('crypto');
const fs = require('fs');
const path = require('path');
const http = require('http');
require('dotenv').config();
const cloudinary = require('cloudinary').v2;


const app = express();
const port = 3000;


const { storage, getImage } = require('./storage/storage');
const upload = multer({ storage: storage });

// Configuración de conexión a la base de datos PostgreSQL

//Dev
//const pool = new Pool({
//  user: 'postgres',
//  host: 'localhost',
//  database: 'MBM',
//  password: 'root',
//  port: 5432, // El puerto por defecto de PostgreSQL
//});

//Prod
const pool = mysql.createPool({
  user: 'u259388479_root',
  host: '145.14.152.202',
  database: 'u259388479_MBM',
  password: 'Crewmeb123*',
  port: 3306,
});



// Configura CORS para permitir solicitudes desde un origen específico (por ejemplo, http://miapp.com)
const corsOptions = {
  origin: 'srv708.hstgr.io', // Reemplaza con tu origen específico
};

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads/uploadsProfile') // Define la carpeta donde se guardarán los archivos
//   },
//   filename: function (req, file, cb) {
//     cb(null, file.originalname); // Define el nombre del archivo
//   }
// });

const storageProducts = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/uploadsProducts') // Define la carpeta donde se guardarán los archivos
  },
  filename: function (req, file, cb) {
    console.log(file, 'file')
    cb(null, file); // Define el nombre del archivo
  }
});

const uploadProducts = multer({ storage: storageProducts });

app.use(session({
  secret: 'secret',

  resave: false,

  saveUninitialized: false
}))

app.use(flash())
app.use(express.urlencoded({ extended: false }))
app.use(cors());
const hola = express();


app.use(express.json());
// Configura el middleware para analizar el cuerpo de la solicitud POST
app.use(bodyParser.json());
app.use('/uploads/uploadsProfile', express.static(path.join(__dirname, 'uploads', 'uploadsProfile')));
app.use('/uploads/uploadsProducts', express.static(path.join(__dirname, 'uploads', 'uploadsProducts')));


const storageNew = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/uploadsProducts/');
  },
  filename: function (req, file, cb) {
    // Cambia el nombre del archivo para incluir la extensión original
    const filename = Date.now() + '-' + file.originalname;
    cb(null, filename);
  }
});

const uploadNew = multer({ storage: storageNew });

// Ruta para manejar la solicitud de carga de archivos
// app.post('/api/upload', uploadNew.array('images'),  async (req, res) =>  {
//   console.log('hola')
//   // Accede a los archivos subidos a través de req.files
//   try {
//     const files = req.files;
//     const { idProducto } = req.body;

//     const uploadedFiles = files.map(file => ({
//       name: file.filename,
//       extension: path.extname(file.originalname) // Obtiene la extensión del archivo
//     }));

//     const urls = uploadedFiles.map(req => req.name);

//     await new Promise((resolve, reject) => {
//       pool.query(
//         'UPDATE producto SET images = ? WHERE id = ?',
//         [JSON.stringify(urls), idProducto[0]],
//         (error, results) => {
//           if (error) {
//             console.error(error);
//             reject(error);
//           } else {
//             resolve();
//           }
//         }
//       );
//     });

//     res.status(200).json({ message: 'Campo images actualizado exitosamente' });
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Error en el servidor al actualizar el campo images.' });
//   }
// });

// app.post("/api/upload", upload.array("images", 10), async (req, res) => {
//   // console.log(req.files)
//   try {
//     let pictureFiles = req.files;
//     //Check if files exist
//     if (!pictureFiles)
//       return res.status(400).json({ message: "No picture attached!" });
//     //map through images and create a promise array using cloudinary upload function
//     let multiplePicturePromise = pictureFiles.map((picture) =>
//       cloudinary.uploader.upload(picture.path)
//     );
//     // await all the cloudinary upload functions in promise.all, exactly where the magic happens
//     let imageResponses = await Promise.all(multiplePicturePromise);

//     const { idProducto } = req.body;

//     const uploadedFiles = pictureFiles.map(file => ({
//       name: file.filename,
//       extension: file.path // Obtiene la extensión del archivo
//     }));

//     const urls = uploadedFiles.map(req => req.extension);

//     await new Promise((resolve, reject) => {
//       pool.query(
//         'UPDATE producto SET images = ? WHERE id = ?',
//         [JSON.stringify(urls), idProducto[0]],
//         (error, results) => {
//           if (error) {
//             console.error(error);
//             reject(error);
//           } else {
//             resolve();
//           }
//         }
//       );
//     });

//     res.status(200).json({ message: 'Campo images actualizado exitosamente' });
//   } catch (err) {
//     res.status(500).json({
//       message: err.message,
//     });
//   }
// });

app.post("/api/upload", upload.array("images", 10), async (req, res) => {
  try {
    let pictureFiles = req.files;

    if (!pictureFiles)
      return res.status(400).json({ message: "No picture attached!" });

    // Divide las imágenes en grupos de a dos
    const groupsOfTwo = [];
    for (let i = 0; i < pictureFiles.length; i += 2) {
      groupsOfTwo.push(pictureFiles.slice(i, i + 2));
    }

    // Sube cada grupo de imágenes
    const imageResponses = [];
    for (const group of groupsOfTwo) {
      const promises = group.map((picture) =>
        cloudinary.uploader.upload(picture.path)
      );
      const responses = await Promise.all(promises);
      imageResponses.push(...responses);
    }

    const { idProducto } = req.body;

    const uploadedFiles = imageResponses.map((file) => ({
      name: file.original_filename,
      extension: file.secure_url,
    }));

    const urls = uploadedFiles.map((req) => req.extension);

    await new Promise((resolve, reject) => {
      pool.query(
        'UPDATE producto SET images = ? WHERE id = ?',
        [JSON.stringify(urls), idProducto[0]],
        (error, results) => {
          if (error) {
            console.error(error);
            reject(error);
          } else {
            resolve();
          }
        }
      );
    });

    res
      .status(200)
      .json({ message: "Campo images actualizado exitosamente" });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
});

// Ruta para obtener datos de la base de datos
app.get('/api/data', validateToken, (req, res) => {
  pool.query('SELECT * FROM usuario', (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener datos' });
      return;
    }
    res.json(results);
  });
});

// Ruta para el inicio de sesión
app.options('/api/login', cors());
app.post('/api/login', (req, res) => {
  const { email, password } = req.body;
  // Consulta la base de datos para encontrar el usuario por su nombre de usuario
  pool.query('SELECT * FROM usuario WHERE email = ?', [email], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al buscar el usuario' });
      return;
    }

    // Verifica si se encontró un usuario
    if (results.length === 0) {
      res.status(401).json({ error: 'Credenciales incorrectas' });
      return;
    }
    const user = results[0];
    // Compara la contraseña proporcionada con la contraseña almacenada en la base de datos
    bcrypt.compare(password, user.password, (err, result) => {
      if (err) {
        console.error(err);
        res.status(500).json({ error: 'Error al verificar la contraseña' });
        return;
      }

      if (result) {
        // Contraseña válida, el usuario está autenticado
        const accessToken = generateAccessToken(user);

        if (accessToken != 'undefined') {
          res.json({ message: 'Inicio de sesión exitoso', token: accessToken, username: user.username, email: user.email, role: user.role });
        }
      } else {
        // Contraseña incorrecta
        res.status(401).json({ error: 'Credenciales incorrectas' });
      }
    });
  });
});

function generateAccessToken(data) {
  const dataStringify = JSON.stringify(data);
  const user = JSON.parse(dataStringify);
  const secretKey = 'NACNAVP';
  return jwt.sign(user, secretKey, { expiresIn: 3600 })
}

function validateToken(req, res, next) {
  // Obtén el token del encabezado de la solicitud
  const token = req.header('Authorization');

  // Verifica si el token está presente
  if (!token) {
    return res.status(401).json({ error: 'Acceso no autorizado' });
  }

  try {
    let tokenValue = token; // Inicialmente, asume que el token no tiene prefijo "Bearer"

    // Verifica si el token comienza con "Bearer "
    if (token.startsWith('Bearer ')) {
      // Si comienza con "Bearer ", elimina ese prefijo
      tokenValue = token.slice(7); // Elimina los primeros 7 caracteres ("Bearer ")
    }

    // Verifica y decodifica el token
    const secretKey = 'NACNAVP';
    const decoded = jwt.verify(tokenValue, secretKey); // Reemplaza con tu clave secreta
    req.user = decoded; // Almacena la información del usuario en la solicitud
    next(); // Continúa con la ejecución de la ruta protegida
  } catch (error) {
    res.status(401).json({ error: 'Token no válido' });
  }
}

// Ruta para obtener datos de la base de datos
app.options('/api/register', cors());
app.post('/api/register', upload.single('imgProfile'), async (req, res) => {
  const { idenDocument, selectedDocument, name, lastName, phone, address, selectedCity, email, password, role, selectedDepartment, selectedPermissions, imgProfile } = req.body;

  let errors = [];

  if (!name || !email || !password) {
    errors.push({ message: 'Por favor llenar datos' })
  }

  if (password.length < 6) {
    errors.push({ message: 'Debe contener 6 caracteres' })
  }

  if (errors.length > 0) {
    res.status(400).json({ error: errors });
  } else {
    let hashedPassword = await bcrypt.hash(password, 10)
    pool.query('SELECT * FROM usuario WHERE email = ?', [email], (error, results) => {
      if (error) {
        console.error(error);
        res.status(500).json({ error: 'Error en el servidor' });
      }

      if (results.length > 0) {
        res.status(200).json({ message: 'Ya existe un usuario registrado con este correo electrónico.' });
      } else {
        const fechaActual = new Date(); // Obtiene la fecha y hora actual
        if (role === 'ADMIN') {
          const uploadedImage = req.file == undefined ? '' : req.file.filename;
          const permissionsArray = selectedPermissions.split(',');
          const permissionsUser = `{${permissionsArray.join(',')}}`;

          // Formatea la fecha y hora en el formato 'YYYY-MM-DD HH:MI:SS'
          const fechaFormateada = fechaActual.toISOString().slice(0, 19).replace('T', ' ');
          pool.query('INSERT INTO usuario (number_document ,type_document, username, email, password, role, estado, creado, permisos, img, address, city, department, phone, lastName) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING password', [idenDocument, selectedDocument, name, email, hashedPassword, role, 'ACTIVO', fechaFormateada, permissionsUser, uploadedImage, address, selectedCity, selectedDepartment, phone, lastName],
            (error, results) => {
              if (error) {
                console.error(error);
                res.status(500).json({ error: 'Error en el servidor' });
              }
              res.status(200).json({ message: 'Registro exitoso' });
            })
        }

        if (role === 'CLIENTE') {
          // Formatea la fecha y hora en el formato 'YYYY-MM-DD HH:MI:SS'
          const fechaFormateada = fechaActual.toISOString().slice(0, 19).replace('T', ' ');
          pool.query('INSERT INTO usuario (username, email, password, role, estado, creado) VALUES (?, ?, ?, ?, ?, ?) RETURNING password', [name, email, hashedPassword, role, 'ACTIVO', fechaFormateada],
            (error, results) => {
              if (error) {
                console.error(error);
                res.status(500).json({ error: 'Error en el servidor' });
              }
              res.status(200).json({ message: 'Registro exitoso' });
            })
        }
      }
    });
  }
});

app.options('/api/addProduct', cors());
app.post('/api/addProduct', uploadProducts.array('images'), async (req, res) => {
  const { productId, images, name, weight, description, category, size, shoppifyHandle, facebookAccount, instagramAccount, price, currency, sku } = req.body;


  const urls = images.map(image => 'image.file');


  // Verifica si todos los campos obligatorios están presentes
  if (!name || !price || !currency || !sku) {
    return res.status(400).json({ error: 'Por favor, complete todos los campos obligatorios.' });
  }

  pool.query('SELECT * FROM producto WHERE id = ?', [productId], (error, resultsPro) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error en el servidor' });
    }

    // Si el producto ya existe
    if (resultsPro.length > 0) {
      pool.query(
        'UPDATE producto SET name = ?, weight = ?, description = ?, category = ?, size = ?, shoppifyHandle = ?, facebookAccount = ?, instagramAccount = ?, price = ?, currency = ?, id = ? WHERE id = ?',
        [name, weight, description, category, size, shoppifyHandle, facebookAccount, instagramAccount, price, currency, sku, productId],
        (error, results) => {
          if (error) {
            console.error(error);
            return res.status(500).json({ error: 'Error en el servidor al actualizar el producto.' });
          }
          return res.status(200).json({ message: 'Producto actualizado exitosamente' });
        }
      );
    } else {
      // Realiza la inserción en la base de datos
      pool.query(
        'INSERT INTO producto (images, name, weight, description, category, size, shoppifyHandle, facebookAccount, instagramAccount, price, currency, sku) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id',
        [JSON.stringify(urls), name, weight, description, category, size, shoppifyHandle, facebookAccount, instagramAccount, price, currency, sku],
        (error, results) => {
          if (error) {
            console.error(error);
            return res.status(500).json({ error: 'Error en el servidor al agregar el producto.' });
          }
          const productId = results[0].id;
          return res.status(200).json({ message: 'Producto agregado exitosamente', productId: productId });
        }
      );
    }
  });
});

// Ruta para obtener datos de los usuarios
app.get('/api/users', validateToken, (req, res) => {
  pool.query('SELECT id, username, email, role, estado, creado, permisos, img FROM usuario ORDER BY id DESC', (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener datos' });
      return;
    }
    res.json(results);
  });
});

// Ruta para obtener datos de los usuarios
app.get('/api/auth', validateToken, (req, res) => {
  res.status(200).json({ message: 'Sesión de usuario activa' });
});

// Implementa la ruta de cierre de sesión

app.post('/api/logout', (req, res) => {
  res.status(200).json({ message: 'Sesión cerrada exitosamente' });
});

// Consultar los menus del Dasboard
app.get('/api/menuMaestro', validateToken, (req, res) => {
  const usuario = req.user.permisos;

  // Eliminamos los corchetes y dividimos la cadena por las comas
  const entidadesArray = usuario.replace(/[{}]/g, '').split(',');

  // Mapeamos cada entidad y la envolvemos con comillas simples
  const entidadesConComillas = entidadesArray.map(entidad => `${entidad.trim()}`);

  const valoresParaIN = `('${entidadesConComillas.join("','")}')`;

  const query = `
    SELECT mp.id AS padre_id,
          mp.nombre AS padre_nombre,
          mp.url AS padre_url,
          mh.id AS hijo_id,
          mh.nombre AS hijo_nombre,
          mh.url AS hijo_url
    FROM menu_padre AS mp
    LEFT JOIN menu_hijo AS mh ON mp.id = mh.padre_id
    WHERE mp.nombre IN ${valoresParaIN}
    ORDER BY mp.id ASC;`;

  pool.query(query, (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener datos' });
      return;
    }
    res.json(results);
  });
});

app.get('/api/imgProfile', validateToken, (req, res) => {
  const idUsuario = req.user.id
  pool.query('SELECT id, img FROM usuario WHERE id = ?', [idUsuario], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener datos' });
      return;
    }
    res.json(results);
  });
});

app.listen(port, () => {
  console.log(`Servidor Node.js en funcionamiento en el puerto ${port}`);
});

// Ruta para obtener datos de los usuarios
app.get('/api/listProducts', (req, res) => {
  pool.query('SELECT * FROM producto ORDER BY id DESC', (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener datos' });
      return;
    }
    res.json(results);
  });
});


// Ruta para el inicio de sesión
app.options('/api/auth/login', cors());
app.post('/api/auth/login', (req, res) => {
  const { email, password } = req.body;
  // Consulta la base de datos para encontrar el usuario por su nombre de usuario
  pool.query('SELECT * FROM usuario WHERE email = ?', [email], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al buscar el usuario' });
      return;
    }

    // Verifica si se encontró un usuario
    if (results.length === 0) {
      res.status(401).json({ error: 'Credenciales incorrectas' });
      return;
    }
    const user = results[0];
    // Compara la contraseña proporcionada con la contraseña almacenada en la base de datos
    bcrypt.compare(password, user.password, (err, result) => {
      if (err) {
        console.error(err);
        res.status(500).json({ error: 'Error al verificar la contraseña' });
        return;
      }

      if (result) {
        // Contraseña válida, el usuario está autenticado
        const accessToken = generateAccessToken(user);

        if (accessToken != 'undefined') {
          res.json({ message: 'Inicio de sesión exitoso', user: { username: user.username, email: user.email, role: user.role }, token: accessToken });
        }
      } else {
        // Contraseña incorrecta
        res.status(401).json({ error: 'Credenciales incorrectas' });
      }
    });
  });
});


app.options('/api/addNewProduct', cors());
app.post('/api/addNewProduct', async (req, res) => {
  const {title,price,description,type,brand,collection,category,sale,discount, stock, new: newProduct, tag} = req.body;
  const dataCollection = Array.isArray(collection) && collection.length === 0 ? '' : collection;
  const dataTags = Array.isArray(tag) && tag.length === 0 ? '' : tag;
  const dataNew = newProduct == false ? '0' : '1';
  const dataSale = sale == false ? '0' : '1';

  // Realiza la inserción en la base de datos
  pool.query(
    'INSERT INTO Product (title,price,description,type,brand,collection,category,sale,discount, stock, new, tags) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id',
    [ title,price,description,type,brand,dataCollection,category,dataSale,discount, stock, dataNew, dataTags],
    (error, results) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: 'Error en el servidor al agregar el producto.' });
      }
      const productId = results[0].id;
      return res.status(200).json({ message: 'Producto agregado exitosamente', productId: productId });
    }
  );
});

app.options('/api/addImages', cors());
app.post('/api/addImages', async (req, res) => {

  const { imageUrls, productoId } = req.body;
  const variant_id = '';
  const alt ='black';

  const url = `["${imageUrls.join('","')}"]`;

  // Realiza la inserción en la base de datos
  pool.query(
    'INSERT INTO Images (variant_id, alt, src, product_id) VALUES (?, ?, ?, ?) RETURNING id',
    [ variant_id, alt,url, productoId],
    (error, results) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: 'Error en el servidor al cargar la imagen.' });
      }
      const imgId = results[0].id;
      return res.status(200).json({ message: 'Imagen cargada exitosamente', imgId: imgId });
    }
  );
});

app.options('/api/addNewVariants', cors());
app.post('/api/addNewVariants', async (req, res) => {

  const { newVariants, productoId, imgId } = req.body;

  const size = newVariants[0].size;
  const sku = newVariants[0].sku;
  const stock = newVariants[0].stock;
  const color = 'black';

  // Realiza la inserción en la base de datos
  pool.query(
    'INSERT INTO Variants (product_id, sku, size, color, image_id) VALUES (?, ?, ?, ?, ?) RETURNING id',
    [ productoId, sku, size, color, imgId ],
    (error, results) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: 'Error en el servidor al agregar los variants.' });
      }
      return res.status(200).json({ message: 'Variants agregado correctamente'});
    }
  );
});